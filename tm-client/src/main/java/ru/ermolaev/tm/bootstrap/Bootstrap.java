package ru.ermolaev.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.ICommandRepository;
import ru.ermolaev.tm.api.ICommandService;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.api.ServiceLocator;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.*;
import ru.ermolaev.tm.exception.UnknownCommandException;
import ru.ermolaev.tm.exception.EmptyCommandException;
import ru.ermolaev.tm.repository.CommandRepository;
import ru.ermolaev.tm.service.CommandService;
import ru.ermolaev.tm.service.SessionService;
import ru.ermolaev.tm.util.TerminalUtil;

import java.lang.Exception;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ISessionService sessionService = new SessionService();

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final AdminDataEndpointService adminDataEndpointService = new AdminDataEndpointService();

    @NotNull
    private final AdminDataEndpoint adminDataEndpoint = adminDataEndpointService.getAdminDataEndpointPort();

    @NotNull
    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private void initCommands(@NotNull final List<AbstractCommand> commandList) {
        for (@NotNull AbstractCommand command: commandList) init(command);
    }

    private void init(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commands.put(command.commandName(), command);
        arguments.put(command.arg(), command);
    }

    @SneakyThrows
    public void run(@Nullable final String[] args) {
        System.out.println("Welcome to task manager");
        initCommands(commandService.getCommandList());
        if (parseArgs(args)) System.exit(0);
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        if (arg == null) return false;
        try {
            parseArg(arg);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }
        return true;
    }

    public void parseArg(@NotNull final String arg) throws Exception {
        if (arg.isEmpty()) return;
        @Nullable final AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new ru.ermolaev.tm.exception.UnknownArgumentException(arg);
        argument.execute();
    }

    public void parseCommand(@Nullable final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) throw new EmptyCommandException();
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    @NotNull
    @Override
    public SessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @NotNull
    @Override
    public AdminDataEndpoint getAdminDataEndpoint() {
        return adminDataEndpoint;
    }

    @NotNull
    @Override
    public AdminUserEndpoint getAdminUserEndpoint() {
        return adminUserEndpoint;
    }

    @NotNull
    @Override
    public TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @NotNull
    @Override
    public ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @NotNull
    @Override
    public UserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

}

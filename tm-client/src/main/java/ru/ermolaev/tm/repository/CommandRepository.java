package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.ermolaev.tm.api.ICommandRepository;
import ru.ermolaev.tm.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        try {
            initCommands();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private void initCommands() throws Exception {
        @NotNull final Reflections reflections = new Reflections("ru.ermolaev.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.ermolaev.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            commandList.add(clazz.newInstance());
        }
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }

}

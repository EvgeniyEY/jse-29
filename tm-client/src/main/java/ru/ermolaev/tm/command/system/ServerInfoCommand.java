package ru.ermolaev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.enumeration.Role;

public final class ServerInfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "server-info";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show information about connecting to server.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SERVER INFO]");
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        @NotNull final String host = serviceLocator.getSessionEndpoint().getServerHost(session);
        @NotNull final Integer port = serviceLocator.getSessionEndpoint().getServerPort(session);
        System.out.println("HOST: " + host);
        System.out.println("PORT: " + port);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}

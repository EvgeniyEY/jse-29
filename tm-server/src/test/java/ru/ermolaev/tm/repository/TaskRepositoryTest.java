package ru.ermolaev.tm.repository;

import org.junit.*;
import ru.ermolaev.tm.api.repository.ITaskRepository;
import ru.ermolaev.tm.dto.TaskDTO;
import ru.ermolaev.tm.exception.unknown.UnknownIdException;
import ru.ermolaev.tm.exception.unknown.UnknownIndexException;
import ru.ermolaev.tm.exception.unknown.UnknownNameException;

import java.util.Arrays;
import java.util.List;

public class TaskRepositoryTest {

    /*
        Требуется рефакторинг в связи с переходом на новую моель хранения данных
    */

//    final private ITaskRepository taskRepository = new TaskRepository();
//
//    final private static TaskDTO TASK_DTO_ONE = new TaskDTO();
//
//    final private static TaskDTO TASK_DTO_TWO = new TaskDTO();
//
//    final private static TaskDTO TASK_DTO_THREE = new TaskDTO();
//
//    final private static TaskDTO[] TASK_DTO_ARRAY = {TASK_DTO_ONE, TASK_DTO_TWO, TASK_DTO_THREE};
//
//    final private static List<TaskDTO> TASK_DTO_LIST = Arrays.asList(TASK_DTO_ARRAY);
//
//    @BeforeClass
//    public static void initData() {
//        TASK_DTO_ONE.setName("task-1");
//        TASK_DTO_ONE.setUserId("123123123");
//
//        TASK_DTO_TWO.setName("task-2");
//        TASK_DTO_TWO.setUserId("123123123");
//
//        TASK_DTO_THREE.setName("task-3");
//        TASK_DTO_THREE.setUserId("789789789789");
//    }
//
//    @Before
//    public void addTask() {
//        taskRepository.add(TASK_DTO_ONE);
//    }
//
//    @After
//    public void deleteTask() {
//        taskRepository.clear();
//    }
//
//    @Test
//    public void findByIdTest() throws Exception {
//        final TaskDTO taskDTO = taskRepository.findById(TASK_DTO_ONE.getUserId(), TASK_DTO_ONE.getId());
//        Assert.assertNotNull(taskDTO);
//        Assert.assertEquals(taskDTO.getName(), TASK_DTO_ONE.getName());
//    }
//
//    @Test
//    public void findByIndexTest() throws Exception {
//        final TaskDTO taskDTO = taskRepository.findByIndex(TASK_DTO_ONE.getUserId(),0);
//        Assert.assertNotNull(taskDTO);
//        Assert.assertEquals(taskDTO.getName(), TASK_DTO_ONE.getName());
//    }
//
//    @Test
//    public void findByNameTest() throws Exception {
//        final TaskDTO taskDTO = taskRepository.findByName(TASK_DTO_ONE.getUserId(), TASK_DTO_ONE.getName());
//        Assert.assertNotNull(taskDTO);
//        Assert.assertEquals(taskDTO.getName(), TASK_DTO_ONE.getName());
//    }
//
//    @Test
//    public void findAllTest() {
//        taskRepository.add(TASK_DTO_LIST);
//        final List<TaskDTO> taskDTOS = taskRepository.findAllTasks(TASK_DTO_ONE.getUserId());
//        Assert.assertNotNull(taskDTOS);
//        Assert.assertEquals(taskDTOS.size(), 3);
//    }
//
//    @Test
//    public void removeByIdTest() throws Exception {
//        Assert.assertEquals(1, taskRepository.findAllTasks(TASK_DTO_ONE.getUserId()).size());
//        taskRepository.removeById(TASK_DTO_ONE.getUserId(), TASK_DTO_ONE.getId());
//        Assert.assertEquals(0, taskRepository.findAllTasks(TASK_DTO_ONE.getUserId()).size());
//    }
//
//    @Test
//    public void removeByIndexTest() throws Exception {
//        Assert.assertEquals(1, taskRepository.findAllTasks(TASK_DTO_ONE.getUserId()).size());
//        taskRepository.removeByIndex(TASK_DTO_ONE.getUserId(), 0);
//        Assert.assertEquals(0, taskRepository.findAllTasks(TASK_DTO_ONE.getUserId()).size());
//    }
//
//    @Test
//    public void removeByNameTest() throws Exception {
//        Assert.assertEquals(1, taskRepository.findAllTasks(TASK_DTO_ONE.getUserId()).size());
//        taskRepository.removeByName(TASK_DTO_ONE.getUserId(), TASK_DTO_ONE.getName());
//        Assert.assertEquals(0, taskRepository.findAllTasks(TASK_DTO_ONE.getUserId()).size());
//    }
//
//    @Test
//    public void removeTest() {
//        Assert.assertEquals(1, taskRepository.findAllTasks(TASK_DTO_ONE.getUserId()).size());
//        taskRepository.remove(TASK_DTO_ONE);
//        Assert.assertEquals(0, taskRepository.findAllTasks(TASK_DTO_ONE.getUserId()).size());
//    }
//
//    @Test
//    public void clearTest() {
//        taskRepository.add(TASK_DTO_LIST);
//        Assert.assertEquals(3, taskRepository.findAllTasks(TASK_DTO_ONE.getUserId()).size());
//        taskRepository.clear(TASK_DTO_ONE.getUserId());
//        Assert.assertEquals(0, taskRepository.findAllTasks(TASK_DTO_ONE.getUserId()).size());
//    }
//
//    @Test
//    public void addTest() throws Exception {
//        taskRepository.add(TASK_DTO_THREE);
//        final TaskDTO taskDTO = taskRepository.findById(TASK_DTO_THREE.getUserId(), TASK_DTO_THREE.getId());
//        Assert.assertNotNull(taskDTO);
//        Assert.assertEquals(taskDTO.getName(), TASK_DTO_THREE.getName());
//    }
//
//    @Test
//    public void loadCollectionTest() {
//        Assert.assertEquals(1, taskRepository.findAll().size());
//        taskRepository.load(TASK_DTO_LIST);
//        Assert.assertEquals(3, taskRepository.findAll().size());
//    }
//
//    @Test
//    public void loadVarargsTest() {
//        Assert.assertEquals(1, taskRepository.findAll().size());
//        taskRepository.load(TASK_DTO_ARRAY);
//        Assert.assertEquals(3, taskRepository.findAll().size());
//    }
//
//    @Test(expected = UnknownIdException.class)
//    public void findByIdExceptionTest() throws Exception {
//        final TaskDTO taskDTO = taskRepository.findById(TASK_DTO_ONE.getUserId(), "qweq");
//        Assert.assertNotNull(taskDTO);
//        Assert.assertEquals(taskDTO.getName(), TASK_DTO_ONE.getName());
//    }
//
//    @Test(expected = UnknownIndexException.class)
//    public void findByIndexExceptionTest() throws Exception {
//        final TaskDTO taskDTO = taskRepository.findByIndex(TASK_DTO_ONE.getUserId(),-7);
//        Assert.assertNotNull(taskDTO);
//        Assert.assertEquals(taskDTO.getName(), TASK_DTO_ONE.getName());
//    }
//
//    @Test(expected = UnknownNameException.class)
//    public void findByNameExceptionTest() throws Exception {
//        final TaskDTO taskDTO = taskRepository.findByName(TASK_DTO_ONE.getUserId(), "qqw");
//        Assert.assertNotNull(taskDTO);
//        Assert.assertEquals(taskDTO.getName(), TASK_DTO_ONE.getName());
//    }
//
//    @Test(expected = UnknownIdException.class)
//    public void removeByIdExceptionTest() throws Exception {
//        Assert.assertEquals(1, taskRepository.findAllTasks(TASK_DTO_ONE.getUserId()).size());
//        taskRepository.removeById(TASK_DTO_ONE.getUserId(), "qwq");
//        Assert.assertEquals(0, taskRepository.findAllTasks(TASK_DTO_ONE.getUserId()).size());
//    }
//
//    @Test(expected = UnknownIndexException.class)
//    public void removeByIndexExceptionTest() throws Exception {
//        Assert.assertEquals(1, taskRepository.findAllTasks(TASK_DTO_ONE.getUserId()).size());
//        taskRepository.removeByIndex(TASK_DTO_ONE.getUserId(), -54);
//        Assert.assertEquals(0, taskRepository.findAllTasks(TASK_DTO_ONE.getUserId()).size());
//    }
//
//    @Test(expected = UnknownNameException.class)
//    public void removeByNameExceptionTest() throws Exception {
//        Assert.assertEquals(1, taskRepository.findAllTasks(TASK_DTO_ONE.getUserId()).size());
//        taskRepository.removeByName(TASK_DTO_ONE.getUserId(), "12wqe");
//        Assert.assertEquals(0, taskRepository.findAllTasks(TASK_DTO_ONE.getUserId()).size());
//    }

}

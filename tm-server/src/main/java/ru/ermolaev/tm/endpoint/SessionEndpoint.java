package ru.ermolaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.endpoint.ISessionEndpoint;
import ru.ermolaev.tm.api.service.ServiceLocator;
import ru.ermolaev.tm.dto.Fail;
import ru.ermolaev.tm.dto.Result;
import ru.ermolaev.tm.dto.SessionDTO;
import ru.ermolaev.tm.dto.Success;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class SessionEndpoint implements ISessionEndpoint {

    private ServiceLocator serviceLocator;

    public SessionEndpoint() {
    }

    public SessionEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) throws Exception {
        return serviceLocator.getSessionService().open(login, password);
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeSession(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        try {
            serviceLocator.getSessionService().close(sessionDTO);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeAllSessionsForUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        try {
            serviceLocator.getSessionService().closeAll(sessionDTO);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Nullable
    @Override
    @WebMethod
    public String getServerHost(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getPropertyService().getServerHost();
    }

    @Nullable
    @Override
    @WebMethod
    public Integer getServerPort(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getPropertyService().getServerPort();
    }

}

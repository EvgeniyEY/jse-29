package ru.ermolaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.endpoint.IProjectEndpoint;
import ru.ermolaev.tm.api.service.ServiceLocator;
import ru.ermolaev.tm.dto.ProjectDTO;
import ru.ermolaev.tm.dto.SessionDTO;
import ru.ermolaev.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@WebService
public final class ProjectEndpoint implements IProjectEndpoint {

    private ServiceLocator serviceLocator;

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void createProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().createProject(sessionDTO.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public void updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().updateById(sessionDTO.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateProjectStartDate(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "date", partName = "date") @Nullable final Date date
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().updateStartDate(sessionDTO.getUserId(), id, date);
    }

    @Override
    @WebMethod
    public void updateProjectCompleteDate(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "date", partName = "date") @Nullable final Date date
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().updateCompleteDate(sessionDTO.getUserId(), id, date);
    }

    @NotNull
    @Override
    @WebMethod
    public Long countAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        return serviceLocator.getProjectService().countAllProjects();
    }

    @NotNull
    @Override
    @WebMethod
    public Long countUserProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getProjectService().countUserProjects(sessionDTO.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO findProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        return ProjectDTO.toDTO(serviceLocator.getProjectService().findOneById(sessionDTO.getUserId(), id));
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO findProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        return ProjectDTO.toDTO(serviceLocator.getProjectService().findOneByName(sessionDTO.getUserId(), name));
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> findAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        return serviceLocator.getProjectService().findAll();
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> findAllProjectsByUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getProjectService().findAllByUserId(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().removeOneById(sessionDTO.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().removeOneByName(sessionDTO.getUserId(), name);
    }

    @Override
    @WebMethod
    public void clearProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getProjectService().removeAll();
    }

    @Override
    @WebMethod
    public void clearProjectsByUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().removeAllByUserId(sessionDTO.getUserId());
    }

}

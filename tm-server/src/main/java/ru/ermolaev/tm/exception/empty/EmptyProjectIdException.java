package ru.ermolaev.tm.exception.empty;

import ru.ermolaev.tm.exception.AbstractException;

public final class EmptyProjectIdException extends AbstractException {

    public EmptyProjectIdException() {
        super("Error! Project ID is empty.");
    }

}

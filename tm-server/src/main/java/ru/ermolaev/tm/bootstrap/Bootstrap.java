package ru.ermolaev.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.api.endpoint.*;
import ru.ermolaev.tm.api.service.*;
import ru.ermolaev.tm.endpoint.*;
import ru.ermolaev.tm.service.*;

import javax.xml.ws.Endpoint;

public final class Bootstrap implements ServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(this);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(this);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(taskService, projectService, userService);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService( this);

    @Getter
    @NotNull
    private final BackupService backupService = new BackupService(this);

    @Getter
    @NotNull
    private final ISqlConnectionService sqlConnectionService = new SqlConnectionService(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final IAdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    @NotNull
    private final IAdminDataEndpoint adminDataEndpoint = new AdminDataEndpoint(this);

    private void initEndpoint() {
        final String url = "http://" + propertyService.getServerHost() + ":" + propertyService.getServerPort();

        final String sessionEndpointUrl = url + "/SessionEndpoint?wsdl";
        Endpoint.publish(sessionEndpointUrl, sessionEndpoint);
        System.out.println(sessionEndpointUrl);

        final String taskEndpointUrl = url + "/TaskEndpoint?wsdl";
        Endpoint.publish(taskEndpointUrl, taskEndpoint);
        System.out.println(taskEndpointUrl);

        final String projectEndpointUrl = url + "/ProjectEndpoint?wsdl";
        Endpoint.publish(projectEndpointUrl, projectEndpoint);
        System.out.println(projectEndpointUrl);

        final String userEndpointUrl = url + "/UserEndpoint?wsdl";
        Endpoint.publish(userEndpointUrl, userEndpoint);
        System.out.println(userEndpointUrl);

        final String adminUserEndpointUrl = url + "/AdminEndpoint?wsdl";
        Endpoint.publish(adminUserEndpointUrl, adminUserEndpoint);
        System.out.println(adminUserEndpointUrl);

        final String adminDataEndpointUrl = url + "/AdminDataEndpoint?wsdl";
        Endpoint.publish(adminDataEndpointUrl, adminDataEndpoint);
        System.out.println(adminDataEndpointUrl);
    }

    @SneakyThrows
    public void run() {
        System.out.println("Starting a task-manager server");
        initEndpoint();
    }

}

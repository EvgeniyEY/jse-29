package ru.ermolaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    Long count();

    @Nullable
    User findOneById(@NotNull String id);

    @Nullable
    User findOneByLogin(@NotNull String login);

    @NotNull
    List<User> findAll();

    void removeOneById(@NotNull String id);

    void removeOneByLogin(@NotNull String login);

    void removeAll();

}
